﻿using Dental.Db.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dental.Db
{
    public class DentalContext : DbContext
    {
        public DbSet<Appointment> Appointments { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Voucher> Vouchers { get; set; }
        public DbSet<Therapy> Therapies { get; set; }
        public DbSet<HealthRecord> Healthrecords { get; set; }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Data Source=dentalSolution.db");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Appointment>().HasKey(x => x.Id);
            modelBuilder.Entity<Appointment>().HasIndex(x => x.Scheduled).IsUnique();
            modelBuilder.Entity<Customer>().HasKey(x => x.Id);
            modelBuilder.Entity<Voucher>().HasKey(x => x.Id);
            modelBuilder.Entity<Therapy>().HasKey(x => x.Id);
            modelBuilder.Entity<HealthRecord>().HasKey(x=> x.Id);
        }
    }
}
