﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dental.Db.Model
{
    public class Appointment
    {
        public int Id { get; set; }
        public string Details { get; set; }
        public int CustomerId { get; set; }
        public virtual Customer Customer { get; set; }
        public int TherapyId { get; set; }
        public virtual Therapy Therapy { get; set; }
        public DateTime Scheduled { get; set; }
        public int VoucherId { get; set; }
        public virtual Voucher Voucher { get; set; }

        public override string ToString()
        {
            return Id + " " + Details;
        }
    }
}
