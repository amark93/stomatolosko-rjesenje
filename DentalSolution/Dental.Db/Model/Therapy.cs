﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dental.Db.Model
{
    public class Therapy
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Details { get; set; }
        public decimal Cost { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}
