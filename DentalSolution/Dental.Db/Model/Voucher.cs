﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dental.Db.Model
{
    public class Voucher
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Detail { get; set; }
        public double Discount { get; set; }

        public override string ToString()
        {
            return Code;
        }
    }
}
