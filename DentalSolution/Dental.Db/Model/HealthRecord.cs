﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dental.Db.Model
{
    public class HealthRecord
    {
        public int Id { get; set; }
        public string HealthIssues { get; set; }
        public string CheckoutHistory { get; set; }
        public string PrescriptionRecord { get; set; }
        public int CustomerId { get; set; }
        public virtual Customer Customer { get; set; }

        public override string ToString()
        {
            return "HealthRecord for Id "+Id;
        }

    }
}
