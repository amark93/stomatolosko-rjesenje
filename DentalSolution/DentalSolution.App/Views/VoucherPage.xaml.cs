﻿using Dental.Db;
using Dental.Db.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace DentalSolution.App.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class VoucherPage : Page
    {
        public VoucherPage()
        {
            this.InitializeComponent();
            FormFields = new Voucher();
            RefreshList();
        }

        ObservableCollection<Voucher> Vouchers { get; set; }
        Voucher FormFields { get; set; }

        private void Reset()
        {
            FormFields = new Voucher();
            Code.Text = "";
            Detail.Text = "";
            Discount.Text = "";
        }

        private void RefreshList()
        {
            Vouchers = new ObservableCollection<Voucher>();
            using (DentalContext context = new DentalContext())
            {
                List<Voucher> temp = context.Vouchers.ToList();
                foreach (Voucher t in temp)
                {
                    Vouchers.Add(t);
                }
            }

            Listing.ItemsSource = Vouchers;
            Listing.IsItemClickEnabled = true;

        }

        private void SetFields(Voucher voucherrData)
        {
            Code.Text = voucherrData.Code;
            Detail.Text = voucherrData.Detail;
            Discount.Text = voucherrData.Discount.ToString();
        }

        private void SetFieldValues()
        {
            FormFields.Code = Code.Text;
            FormFields.Detail = Detail.Text;
            double i = 0;
            Double.TryParse(Discount.Text, out i);
            FormFields.Discount = i;
        }

        private async void Save_Click(object sender, RoutedEventArgs e)
        {
            SetFieldValues();
            if(String.IsNullOrEmpty(FormFields.Code) || String.IsNullOrEmpty(FormFields.Detail))
            {
                MessageDialog warning = new MessageDialog("Fill out the form");
                await warning.ShowAsync();
                return;
            }

            if(FormFields.Discount < 1 || FormFields.Discount >= 100)
            {
                MessageDialog warning = new MessageDialog("Discount field has to be in between 1 and 99");
                await warning.ShowAsync();
                return;
            }

            Voucher temp = new Voucher();
            using (DentalContext context = new DentalContext())
            {
                if (FormFields.Id == 0 || FormFields == null)
                {
                    context.Vouchers.Add(FormFields);
                }
                else
                {
                    temp = context.Vouchers.Single(x => x.Id == FormFields.Id);
                    temp.Code = FormFields.Code;
                    temp.Detail = FormFields.Detail;
                    temp.Discount = FormFields.Discount;
                }
                await context.SaveChangesAsync();
            }
            RefreshList();
            Reset();
            MessageDialog message = new MessageDialog("Changes are saved");
            await message.ShowAsync();
        }

        private void Delete_Click(object sender, RoutedEventArgs e)
        {
            if (FormFields.Id == 0 || FormFields == null) return;

            using (DentalContext context = new DentalContext())
            {
                context.Remove(FormFields);
                context.SaveChanges();
            }
            RefreshList();
            Reset();
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            Reset();
        }

        private void Listing_ItemClick(object sender, ItemClickEventArgs e)
        {
            FormFields = (Voucher)e.ClickedItem;
            SetFields(FormFields);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (Frame.CanGoBack) Frame.GoBack();
        }
    }
}
