﻿using Dental.Db;
using Dental.Db.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace DentalSolution.App.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class AppointmentPage : Page
    {
        public AppointmentPage()
        {
            this.InitializeComponent();
            FormFields = new Appointment();
            RefreshList();
            Therapies = new ObservableCollection<Therapy>();
            Vouchers = new ObservableCollection<Voucher>();
            Customers = new ObservableCollection<Customer>();
            using (DentalContext context = new DentalContext())
            {
                List<Therapy> temp = context.Therapies.ToList();
                List<Voucher> temp2 = context.Vouchers.ToList();
                List<Customer> temp3 = context.Customers.ToList();

                foreach (Therapy t in temp)
                {
                    Therapies.Add(t);
                }

                foreach (Voucher t in temp2)
                {
                    Vouchers.Add(t);
                }

                foreach (Customer t in temp3)
                {
                    Customers.Add(t);
                }

                Customer.ItemsSource = Customers;
                Therapy.ItemsSource = Therapies;
                Voucher.ItemsSource = Vouchers;

            }
        }

        ObservableCollection<Appointment> Appointments { get; set; }
        ObservableCollection<Customer> Customers { get; set; }
        ObservableCollection<Therapy> Therapies { get; set; }
        ObservableCollection<Voucher> Vouchers { get; set; }
        Appointment FormFields { get; set; }

        private void Reset()
        {
            FormFields = new Appointment();
            Details.Text = "";
            Customer.SelectedItem = null;
            Time.Time = DateTime.Today.TimeOfDay;
            ScheduleDate.Date = DateTime.Today;
            Voucher.SelectedItem = null;
            Therapy.SelectedItem = null;
        }

        private void RefreshList()
        {
            Appointments = new ObservableCollection<Appointment>();
            using (DentalContext context = new DentalContext())
            {
                List<Appointment> temp = context.Appointments.ToList();
                foreach (Appointment t in temp)
                {
                    Appointments.Add(t);
                }
            }

            Listing.ItemsSource = Appointments;
            Listing.IsItemClickEnabled = true;

        }

        private void SetFields(Appointment appointmentData)
        {
            Details.Text = appointmentData.Details;
            Customer.SelectedItem = Customers.Where(x => x.Id == appointmentData.CustomerId).First();
            Time.Time = appointmentData.Scheduled.TimeOfDay;
            ScheduleDate.Date = appointmentData.Scheduled;
            Voucher.SelectedItem = Vouchers.Where(x => x.Id == appointmentData.VoucherId).First();
            Therapy.SelectedItem = Therapies.Where(x => x.Id == appointmentData.TherapyId).First();
        }

        private void SetFieldValues()
        {
            FormFields.Details = Details.Text;

            FormFields.CustomerId = ((Customer)Customer.SelectedValue).Id;

            FormFields.Scheduled = ScheduleDate.Date.Value.Date+ Time.Time;

            FormFields.TherapyId = ((Therapy)Therapy.SelectedValue).Id;

            FormFields.VoucherId = ((Voucher)Voucher.SelectedValue).Id;
        }

        private async void Save_Click(object sender, RoutedEventArgs e)
        {
            if(Customer.SelectedIndex==-1 || Voucher.SelectedIndex==-1 || Therapy.SelectedIndex == -1)
            {
                MessageDialog warning = new MessageDialog("Please select customer, voucher and therapy");
                await warning.ShowAsync();
                return;
            }

            if (ScheduleDate.Date == null || Time.Time == null || ScheduleDate.Date<=DateTime.Now) {
                MessageDialog warning = new MessageDialog("Please fill out schedule time and make sure the date is after today");
                await warning.ShowAsync();
                return;
            }

            SetFieldValues();

            if (String.IsNullOrEmpty(FormFields.Details))
            {
                MessageDialog warning = new MessageDialog("Fill out the form");
                await warning.ShowAsync();
                return;
            }

            Appointment temp = new Appointment();
            using (DentalContext context = new DentalContext())
            {
                if (FormFields.Id == 0 || FormFields == null)
                {
                    context.Appointments.Add(FormFields);
                }
                else
                {
                    temp = context.Appointments.Single(x => x.Id == FormFields.Id);
                    temp.Details = FormFields.Details;
                    temp.Customer = FormFields.Customer;
                    temp.Voucher = FormFields.Voucher;
                    temp.Therapy = FormFields.Therapy;
                    temp.Scheduled = FormFields.Scheduled;
                }
                try {
                    await context.SaveChangesAsync();
                }
                catch {
                    Reset();
                    MessageDialog warning = new MessageDialog("Set date got to be unique");
                    await warning.ShowAsync();
                    return;
                }
            }
            RefreshList();
            Reset();
            MessageDialog message = new MessageDialog("Changes are saved");
            await message.ShowAsync();
        }

        private void Delete_Click(object sender, RoutedEventArgs e)
        {
            if (FormFields.Id == 0 || FormFields == null) return;

            using (DentalContext context = new DentalContext())
            {
                context.Remove(FormFields);
                context.SaveChanges();
            }
            RefreshList();
            Reset();
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            Reset();
        }

        private void Listing_ItemClick(object sender, ItemClickEventArgs e)
        {
            FormFields = (Appointment)e.ClickedItem;
            SetFields(FormFields);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (Frame.CanGoBack) Frame.GoBack();
        }
    }
}
