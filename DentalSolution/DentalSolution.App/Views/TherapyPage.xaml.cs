﻿using Dental.Db;
using Dental.Db.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace DentalSolution.App.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class TherapyPage : Page
    {
        public TherapyPage()
        {
            this.InitializeComponent();
            FormFields = new Therapy();
            RefreshList();
        }

        ObservableCollection<Therapy> Therapies { get; set; }
        Therapy FormFields { get; set; }

        private void Reset()
        {
            FormFields = new Therapy();
            TherapyName.Text = "";
            Detail.Text = "";
            Cost.Text = "";
        }

        private void RefreshList()
        {
            Therapies = new ObservableCollection<Therapy>();
            using (DentalContext context = new DentalContext())
            {
                List<Therapy> temp = context.Therapies.ToList();
                foreach (Therapy t in temp)
                {
                    Therapies.Add(t);
                }
            }

            Listing.ItemsSource = Therapies;
            Listing.IsItemClickEnabled = true;

        }

        private void SetFields(Therapy therapyData)
        {
            TherapyName.Text = therapyData.Name;
            Detail.Text = therapyData.Details;
            Cost.Text = therapyData.Cost.ToString();
        }

        private void SetFieldValues()
        {
            FormFields.Name = TherapyName.Text;
            FormFields.Details = Detail.Text;
            Decimal i = 0;
            Decimal.TryParse(Cost.Text, out i);
            FormFields.Cost = i;
        }

        private async void Save_Click(object sender, RoutedEventArgs e)
        {
            SetFieldValues();

            if (String.IsNullOrEmpty(FormFields.Name) || String.IsNullOrEmpty(FormFields.Details)) {
                MessageDialog warning = new MessageDialog("Fill out the form");
                await warning.ShowAsync();
                return;
            }

            if(FormFields.Cost <= 0)
            {
                MessageDialog warning = new MessageDialog("Cost has to be numeric and above 0.");
                await warning.ShowAsync();
                return;
            }

            Therapy temp = new Therapy();
            using (DentalContext context = new DentalContext())
            {
                if (FormFields.Id == 0 || FormFields == null)
                {
                    context.Therapies.Add(FormFields);
                }
                else
                {
                    temp = context.Therapies.Single(x => x.Id == FormFields.Id);
                    temp.Name = FormFields.Name;
                    temp.Details = FormFields.Details;
                    temp.Cost = FormFields.Cost;
                }
                await context.SaveChangesAsync();
            }
            RefreshList();
            Reset();
            MessageDialog message = new MessageDialog("Changes are saved");
            await message.ShowAsync();
        }

        private void Delete_Click(object sender, RoutedEventArgs e)
        {
            if (FormFields.Id == 0 || FormFields == null) return;

            using (DentalContext context = new DentalContext())
            {
                context.Remove(FormFields);
                context.SaveChanges();
            }
            RefreshList();
            Reset();
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            Reset();
        }

        private void Listing_ItemClick(object sender, ItemClickEventArgs e)
        {
            FormFields = (Therapy)e.ClickedItem;
            SetFields(FormFields);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (Frame.CanGoBack) Frame.GoBack();
        }
    }
}
