﻿using Dental.Db;
using Dental.Db.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace DentalSolution.App.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class HealthRecordPage : Page
    {
        public HealthRecordPage()
        {
            this.InitializeComponent();
            FormFields = new HealthRecord();
            RefreshList();
            HealthRecords = new ObservableCollection<HealthRecord>();
            Customers = new ObservableCollection<Customer>();
            using (DentalContext context = new DentalContext()) {
                List<Customer> temp = context.Customers.ToList();
                foreach (Customer t in temp)
                {
                    Customers.Add(t);
                }

                Customer.ItemsSource = Customers;
            }
        }

        ObservableCollection<Customer> Customers { get; set; }
        ObservableCollection<HealthRecord> HealthRecords { get; set; }
        HealthRecord FormFields { get; set; }

        private void Reset()
        {
            FormFields = new HealthRecord();
            HealthIssues.Document.SetText(Windows.UI.Text.TextSetOptions.None, "");
            CheckoutHistory.Document.SetText(Windows.UI.Text.TextSetOptions.None, "");
            PrescriptionHistory.Document.SetText(Windows.UI.Text.TextSetOptions.None, "");
            Customer.SelectedItem = null;
        }

        private void RefreshList()
        {
            HealthRecords = new ObservableCollection<HealthRecord>();
            using (DentalContext context = new DentalContext())
            {
                List<HealthRecord> temp = context.Healthrecords.ToList();
                foreach (HealthRecord t in temp)
                {
                    HealthRecords.Add(t);
                }
            }

            Listing.ItemsSource = HealthRecords;
            Listing.IsItemClickEnabled = true;

        }

        private void SetFields(HealthRecord healthData)
        {

            Customer.SelectedItem = Customers.Where(x => x.Id == healthData.CustomerId).First();
            HealthIssues.Document.SetText(Windows.UI.Text.TextSetOptions.None, healthData.HealthIssues);
            CheckoutHistory.Document.SetText(Windows.UI.Text.TextSetOptions.None, healthData.CheckoutHistory);
            PrescriptionHistory.Document.SetText(Windows.UI.Text.TextSetOptions.None, healthData.PrescriptionRecord);
        }

        private void SetFieldValues()
        {
            String data = "";
            CheckoutHistory.Document.GetText(Windows.UI.Text.TextGetOptions.AdjustCrlf, out data);
            FormFields.CheckoutHistory = data;

            FormFields.CustomerId = ((Customer)Customer.SelectedValue).Id;

            PrescriptionHistory.Document.GetText(Windows.UI.Text.TextGetOptions.AdjustCrlf, out data);
            FormFields.PrescriptionRecord = data;

            HealthIssues.Document.GetText(Windows.UI.Text.TextGetOptions.AdjustCrlf, out data);
            FormFields.HealthIssues = data;

        }

        private async void Save_Click(object sender, RoutedEventArgs e)
        {
            if (Customer.SelectedIndex == -1) {
                MessageDialog warning = new MessageDialog("Please choose a customer");
                await warning.ShowAsync();
                return;
            }
            SetFieldValues();
            if (String.IsNullOrEmpty(FormFields.HealthIssues) ||
               String.IsNullOrEmpty(FormFields.PrescriptionRecord) ||
               String.IsNullOrEmpty(FormFields.CheckoutHistory)) {
                MessageDialog warning = new MessageDialog("Please fill out the form");
                await warning.ShowAsync();
                return;
            }

            HealthRecord temp = new HealthRecord();
            using (DentalContext context = new DentalContext())
            {
                if (FormFields.Id == 0 || FormFields == null)
                {
                    context.Healthrecords.Add(FormFields);
                }
                else
                {
                    temp = context.Healthrecords.Single(x => x.Id == FormFields.Id);
                    temp.Customer = FormFields.Customer;
                    temp.HealthIssues = FormFields.HealthIssues;
                    temp.PrescriptionRecord = FormFields.PrescriptionRecord;
                    temp.CheckoutHistory = FormFields.CheckoutHistory;
                }
                await context.SaveChangesAsync();
            }
            RefreshList();
            Reset();
            MessageDialog message = new MessageDialog("Changes are saved");
            await message.ShowAsync();
        }

        private void Delete_Click(object sender, RoutedEventArgs e)
        {
            if (FormFields.Id == 0 || FormFields == null) return;

            using (DentalContext context = new DentalContext())
            {
                context.Remove(FormFields);
                context.SaveChanges();
            }
            RefreshList();
            Reset();
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            Reset();
        }

        private void Listing_ItemClick(object sender, ItemClickEventArgs e)
        {
            FormFields = (HealthRecord)e.ClickedItem;
            SetFields(FormFields);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (Frame.CanGoBack) Frame.GoBack();
        }
    }
}
