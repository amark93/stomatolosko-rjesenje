﻿using Dental.Db;
using Dental.Db.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text.RegularExpressions;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace DentalSolution.App.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class CustomerPage : Page
    {
        public CustomerPage()
        {
            this.InitializeComponent();
            FormFields = new Customer();
            RefreshList();
        }

        ObservableCollection<Customer> Customers { get; set; }

        Customer FormFields { get; set; }

        private async void Save_Click(object sender, RoutedEventArgs e)
        {
            SetFieldValues();
            if(
               String.IsNullOrEmpty(FormFields.Name) || 
               String.IsNullOrEmpty(FormFields.Lastname) ||
               String.IsNullOrEmpty(FormFields.Address) ||
               String.IsNullOrEmpty(FormFields.Mobile) ||
               String.IsNullOrEmpty(FormFields.Telephone) ||
               String.IsNullOrEmpty(FormFields.Email)
            )
            {
                MessageDialog warning = new MessageDialog("Fill out the form");
                await warning.ShowAsync();
                return;
            }

            if (FormFields.Age < 1)
            {
                MessageDialog warning = new MessageDialog("Age has to be larger than 1");
                await warning.ShowAsync();
                return;
            }

            if(!Regex.Match(FormFields.Telephone, @"^[0-9]*$").Success || !Regex.Match(FormFields.Mobile, @"^[0-9]*$").Success) {
                MessageDialog warning = new MessageDialog("Telephone and mobile have to be numeric");
                await warning.ShowAsync();
                return;
            }

            try
            {
                MailAddress address = new MailAddress(FormFields.Email);
            }
            catch (FormatException)
            {
                MessageDialog warning = new MessageDialog("Email is invalid, type it in correctly.");
                await warning.ShowAsync();
                return;
            }

            Customer temp = new Customer();
            using (DentalContext context = new DentalContext())
            {
                if (FormFields.Id == 0 || FormFields == null)
                {
                    context.Customers.Add(FormFields);
                }
                else
                {
                    temp = context.Customers.Single(x => x.Id == FormFields.Id);
                    temp.Name = FormFields.Name;
                    temp.Lastname = FormFields.Lastname;
                    temp.Age = FormFields.Age;
                    temp.Address = FormFields.Address;
                    temp.Telephone = FormFields.Telephone;
                    temp.Mobile = FormFields.Mobile;
                    temp.Email = FormFields.Email;
                }
                await context.SaveChangesAsync();
            }
            RefreshList();
            Reset();
            MessageDialog message = new MessageDialog("Changes are saved");
            await message.ShowAsync();
        }

        private void SetFields(Customer customerData)
        {
            CustomerName.Text = customerData.Name;
            LastName.Text = customerData.Lastname;
            Age.Text = customerData.Age.ToString();
            Address.Text = customerData.Address;
            Telephone.Text = customerData.Telephone;
            Mobile.Text = customerData.Mobile;
            Email.Text = customerData.Email;
        }

        private void SetFieldValues()
        {
            FormFields.Name = CustomerName.Text;
            FormFields.Lastname = LastName.Text;
            FormFields.Mobile = Mobile.Text;
            FormFields.Telephone = Telephone.Text;
            FormFields.Address = Address.Text;
            FormFields.Email = Email.Text;
            int i = 0;
            Int32.TryParse(Age.Text, out i);
            FormFields.Age = i;
        }

        private void Reset()
        {
            FormFields = new Customer();
            CustomerName.Text = "";
            LastName.Text = "";
            Age.Text = "";
            Telephone.Text = "";
            Mobile.Text = "";
            Address.Text = "";
            Email.Text = "";
        }

        private void RefreshList()
        {
            Customers = new ObservableCollection<Customer>();
            using (DentalContext context = new DentalContext())
            {
                List<Customer> temp = context.Customers.ToList();
                foreach (Customer t in temp)
                {
                    Customers.Add(t);
                }
            }

            Listing.ItemsSource = Customers;
            Listing.IsItemClickEnabled = true;

        }

        private void Delete_Click(object sender, RoutedEventArgs e)
        {
            if (FormFields.Id == 0 || FormFields == null) return;

            using (DentalContext context = new DentalContext())
            {
                context.Remove(FormFields);
                context.SaveChanges();
            }
            RefreshList();
            Reset();
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            Reset();
        }

        private void Listing_ItemClick(object sender, ItemClickEventArgs e)
        {
            FormFields = (Customer)e.ClickedItem;
            SetFields(FormFields);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (Frame.CanGoBack)
            {
                Frame.GoBack();
            }
        }

        private void Listing_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}
