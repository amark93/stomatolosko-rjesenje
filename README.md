# Stomatološko Rješenje

Universal Windows Platform application

Jednostavna UWP aplikacija rađena u C# uz code first approach prema SqlLite bazi.

Ispod je prikaz menija aplikacije.

![alt text](./screens/menuPage.png)

Ispod je prikaz formi gdje se CRUD operacije nad klijentima, njihovima zakazanim pregledima, kuponima, terapijama i historiji bolesti klijenata mogu odvijati.

#Forma za Klijente

![alt text](./screens/customerPage.png)

#Forma za Zakazivanje Pregleda Klijentu

![alt text](./screens/appointmentPage.png)

#Forma za Kupone

![alt text](./screens/voucherPage.png)

#Forma za Terapije

![alt text](./screens/therapyPage.png)

#Forma za Historiju Zdravlja Klijenta

![alt text](./screens/healthPage.png)